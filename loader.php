<?php
class GMCECredLoader {

    private $namespace;

    private $path;

    public function __construct( $ns = null, $path = '' ) {
        $this->namespace = $ns;
        $this->path = $path;
    }

    public function register() {
        spl_autoload_register( array ( $this, 'load' ) );
    }

    public function load( $name ) {
        $name = ltrim( $name, '/\\' );
        if ( ! ( strpos( $name, $this->namespace ) === 0 ) ) return;
        $file = str_replace( '\\', DIRECTORY_SEPARATOR, $name ) . '.php';
        require trailingslashit( $this->path ) . $file;
    }

}