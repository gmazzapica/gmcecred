<?php

namespace GMCECred;

class AdminBox {

    private $user;

    private $settings;

    private $do_save = false;

    function set_data( \GMCE\User $user, \GMCE\SettingsGMCE $settings ) {
        $this->user = $user;
        $this->settings = $settings;
    }

    function setup() {
        add_action( 'gmce_adminbox_tbody_end', array ( $this, 'admin_box' ) );
    }

    function admin_box( $post ) {
        \wp_nonce_field( 'GMCECred', 'gmcecred_nonce' );
        $format = '<input%1$s type="checkbox" value="1" name="%2$s" id="%2$s">&nbsp;';
        $now = \array_filter( (array) \get_post_meta( $post->ID, '__gmcecred_skip', true ) );
        $noup = $nodown = $was = false;
        if ( ! empty( $now ) ) {
            $noup = isset( $now[ 'up' ] ) && ! empty( $now[ 'up' ] );
            $nodown = isset( $now[ 'down' ] ) && ! empty( $now[ 'down' ] );
        } else {
            $was = (int) \get_post_meta( $post->ID, '__gmce_published', true );
        }
        $noupchecked = $noup || $was ? ' checked="checked"' : '';
        $nodownchecked = $nodown || $was ? ' checked="checked"' : '';
        $this->admin_box_html( $format, $noupchecked, $nodownchecked );
    }

    function admin_box_html( $format, $noupchecked, $nodownchecked ) {
        ?>
        <tr valign="top">
            <td colspan="2">
                <h2 style="margin-top:0; padding-top: 0;">
                    <?php _e( 'MyCRED Settings', 'gmcecred' ); ?>
                </h2>
                <p>
                    <label>
                        <?php
                        \printf( $format, $noupchecked, 'gmcecred_no_rw_a' );
                        _e( 'Do NOT Reward Approvers on Publish', 'gmcecred' );
                        ?>
                    </label>
                </p>
                <p>
                    <label>
                        <?php
                        \printf( $format, $nodownchecked, 'gmcecred_no_rw_r' );
                        _e( 'Do NOT Reward Revisers on Publish', 'gmcecred' );
                        ?>
                    </label>
                </p>
            </td>
        </tr>
        <?php
    }

    function should_save( $new_status, $old_status ) {
        $a = ( $old_status === 'draft' || $old_status === 'pending' );
        $b = ( $new_status === 'draft' || $new_status === 'pending' || $new_status === 'publish' );
        $this->do_save = ($a && $b);
    }

    function save_admin_box( $post ) {
        if ( \defined( '\DOING_AUTOSAVE' ) && \DOING_AUTOSAVE ) return;
        if ( ! $this->user->is_admin ) return;
        if ( ! $this->do_save ) return;
        if ( ( \current_filter() === 'save_post' ) && \did_action( 'pending_to_publish' ) ) return;
        $nonce = \filter_input( \INPUT_POST, 'gmcecred_nonce', \FILTER_SANITIZE_STRING );
        if ( empty( $nonce ) || ! \wp_verify_nonce( $nonce, 'GMCECred' ) ) return;
        if ( \is_numeric( $post ) ) $post = \get_post( $post );
        $allowed = $this->settings->get( 'post_types', array ( 'post' ) );
        if ( ! \in_array( $post->post_type, $allowed ) ) return;
        $norew_a = \filter_input( \INPUT_POST, 'gmcecred_no_rw_a', \FILTER_SANITIZE_NUMBER_INT );
        $norew_r = \filter_input( \INPUT_POST, 'gmcecred_no_rw_r', \FILTER_SANITIZE_NUMBER_INT );
        $meta = array (
            'up'   => ( (int) $norew_a === 1 ) ? '1' : '',
            'down' => ( (int) $norew_r === 1 ) ? '1' : ''
        );
        \update_post_meta( $post->ID, '__gmcecred_skip', $meta );
    }

}