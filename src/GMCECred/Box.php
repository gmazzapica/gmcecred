<?php

namespace GMCECred;

class Box {

    function setup() {
        add_action( 'gmce_box_after_approver_actions', array ( $this, 'approver_button' ) );
        add_action( 'gmce_revisionsbox_revision_actions_admin', array ( $this, 'revision_button' ) );
    }

    function approver_button() {
        $f = '<a href="#" class="gmce_approve_punish" title="%s">!</a>';
        printf( $f, __( 'Delete Approval and Punish Author', 'gmcecred' ) );
    }

    function revision_button() {
        $f = '<a href="#" class="gmce_rev_punish" title="%s">!</a>';
        printf( $f, __( 'Delete Revision and Punish Author', 'gmcecred' ) );
    }

}