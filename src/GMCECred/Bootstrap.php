<?php

namespace GMCECred;

use Brain\Container as Brain;

class Bootstrap {

    private $path;

    private $plugin;

    private $hooks;

    private $adminbox;

    private $box;

    private $ok;

    private $user;

    private $settings;

    private $ce;

    private $front;

    private $back;

    private $is_ajax;

    private $post;

    function __construct( Hooks $hooks, Box $box, AdminBox $adminbox ) {
        $this->hooks = $hooks;
        $this->box = $box;
        $this->adminbox = $adminbox;
    }

    function set_path( $path ) {
        $this->path = $path;
        $this->plugin = $this->path . '/gmcecred.php';
    }

    function init() {
        $this->is_ajax = defined( 'DOING_AJAX' ) && DOING_AJAX;
        add_action( 'brain_loaded', array ( $this, 'dependences' ) );
    }

    function dependences() {
        $this->ok = ( class_exists( '\myCRED_Hook' ) && class_exists( '\GMCE\CommunityEditor' ) );
        $this->boot();
    }

    function setup() {
        if ( $this->ok ) {
            $this->ce();
            $this->check_post();
            $this->setup_scripts();
        }
    }

    private function boot() {
        if ( $this->ok ) {
            $path = dirname( plugin_basename( $this->plugin ) ) . '/lang';
            load_plugin_textdomain( 'gmcecred', false, $path );
            $this->hooks->setup( $this->path );
            $this->user = Brain::instance()->get( 'user' );
            $this->settings = Brain::instance()->get( 'settings' );
            $this->adminbox->set_data( $this->user, $this->settings );
            if ( $this->is_ajax ) $this->adminbox->setup();
            if ( is_admin() && ! $this->is_ajax ) {
                add_action( 'load-post.php', array ( $this, 'setup' ), 15 );
            } else {
                add_action( 'init', array ( $this, 'setup' ), 15 );
            }
            add_action( 'transition_post_status', array ( $this->adminbox, 'should_save' ), 1, 2 );
            add_action( 'pending_to_publish', array ( $this->adminbox, 'save_admin_box' ), 1 );
            add_action( 'save_post', array ( $this->adminbox, 'save_admin_box' ) );
            add_action( 'gmce_thumb_voted', array ( $this->hooks, 'thumb_vote' ) );
        }
    }

    private function ce() {
        $this->ce = Brain::instance()->get( 'community_editor' );
        $this->front = $this->ce->get_frontend();
        $this->back = $this->ce->get_backend();
    }

    private function check_post() {
        $post = ( is_admin() && ! $this->is_ajax ) ? $this->back->get_post() : $this->front->get_post();
        $this->post = ! empty( $post ) && ( $this->user->allowed_post->ID === $post->ID ) ? $post : NULL;
        if ( ! $this->user->is_admin ) return;
        $this->box->setup();
        if ( ! $this->is_ajax && is_admin() ) $this->adminbox->setup();
    }

    private function setup_scripts() {
        if ( empty( $this->post ) ) return;
        $this->reg_script();
        $action = is_admin() ? 'admin_enqueue_scripts' : 'wp_enqueue_scripts';
        add_action( $action, array ( $this, 'script' ) );
    }

    function reg_script() {
        $min = defined( 'WP_DEBUG' ) && WP_DEBUG ? '' : '.min';
        $url = plugins_url( "/assets/js/gmce_cred{$min}.js", $this->plugin );
        wp_register_script( 'gmce_cred', $url, array ( 'jquery', 'easyajax', 'gmce' ), null );
    }

    function localize_script() {
        $rev = __( 'Are you sure to delete this revision and punish the author?', 'gmcecred' );
        $app = __( 'Are you sure to delete approval and punish %s?', 'gmcecred' );
        $data = array (
            'conf_punish_rev'     => $rev,
            'conf_punish_approve' => $app
        );
        wp_localize_script( 'gmce_cred', 'GMCECredData', $data );
    }

    function script() {
        if ( ! $this->is_ajax ) {
            wp_enqueue_script( 'gmce_cred' );
            $this->localize_script();
        }
    }

}