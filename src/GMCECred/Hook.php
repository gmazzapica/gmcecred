<?php

namespace GMCECred;

class Hook extends \myCRED_Hook {

    function __construct( $hook_prefs ) {
        $hook = array ( 'id' => static::$hid, 'default' => array ( 'creds' => static::$points, ) );
        parent::__construct( $hook, $hook_prefs );
    }

    function run() {
        add_action( static::$hook, array ( $this, 'reward_or_punish' ) );
    }

    function reward_or_punish( $user_id = 0 ) {
        if ( $this->core->exclude_user( $user_id ) ) return;
        $for = substr_count( current_filter(), 'approval' ) ? 'approval' : 'revision';
        $event = substr_count( current_filter(), 'reward' ) ? 'reward' : 'punish';
        $format = __( '%s for GM Community Editor %s', 'gmcecred' );
        $log = sprintf( $format, ucfirst( $event ), ucfirst( $for ) );
        $points = $this->prefs[ 'creds' ];
        if ( $event === 'punish' && $points > 0 ) $points *= (-1);
        $this->core->add_creds( 'gmce_' . $event . '_for_' . $for, $user_id, $points, $log );
    }

    function preferences() {
        $prefs = $this->prefs;
        echo '<label class="subheader">' . $this->core->plural() . '</label>';
        echo '<ol><li><div class = "h2">';
        $f = '<input type="text" name="%s" id="%s" value="%s" size="8" />';
        $name = $this->field_name( 'creds' );
        $id = $this->field_id( 'creds' );
        \printf( $f, $name, $id, $this->core->format_number( $prefs[ 'creds' ] ) );
        echo '</div></li></ol>';
    }

}