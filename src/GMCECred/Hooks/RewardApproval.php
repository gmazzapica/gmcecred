<?php

namespace GMCECred\Hooks;

class RewardApproval extends \GMCECred\Hook {

    static $hid = 'gmce_reward_approval';

    static $hook = 'gmce_approval_reward';

    static $points = 5;

}