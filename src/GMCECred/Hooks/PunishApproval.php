<?php

namespace GMCECred\Hooks;

class PunishApproval extends \GMCECred\Hook {

    static $hid = 'gmce_punish_approval';

    static $hook = 'gmce_approval_punish';

    static $points = -5;

}