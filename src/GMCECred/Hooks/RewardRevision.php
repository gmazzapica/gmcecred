<?php

namespace GMCECred\Hooks;

class RewardRevision extends \GMCECred\Hook {

    static $hid = 'gmce_reward_revision';

    static $hook = 'gmce_revision_reward';

    static $points = 10;

}