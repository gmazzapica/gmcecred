<?php

namespace GMCECred\Hooks;

class PunishRevision extends \GMCECred\Hook {

    static $hid = 'gmce_punish_revision';

    static $hook = 'gmce_revision_punish';

    static $points = -10;

}