<?php

namespace GMCECred;

class Hooks {

    private $path;

    private $skip = array ();

    function setup( $path ) {
        $this->path = $path;
        add_filter( 'mycred_setup_hooks', array ( $this, 'register' ) );
        add_filter( 'gmce_ajax_actions_setting', array ( $this, 'ajax_actions' ) );
        add_action( 'gmce_confirm_user_vote', array ( $this, 'reward_user' ), 10, 20 );
    }

    function register( $installed ) {
        return array_merge( $installed, $this->get_hooks() );
    }

    function get_hooks() {
        $hooks = array ();
        require( $this->path . '/hooks.php');
        return $hooks;
    }

    function ajax_actions( $actions = array () ) {
        $add = array (
            'GMCECred\Hooks.revision_punish',
            'GMCECred\Hooks.approval_punish'
        );
        return array_merge( $actions, $add );
    }

    static function revision_punish() {
        $id = filter_input( INPUT_POST, 'gmce_uid', FILTER_SANITIZE_NUMBER_INT );
        do_action( "gmce_revision_punish", $id );
    }

    static function approval_punish() {
        $id = filter_input( INPUT_POST, 'gmce_uid', FILTER_SANITIZE_NUMBER_INT );
        do_action( "gmce_approval_punish", $id );
    }

    function reward_user( $userid = 0, $up_down = 'down', $meta = array (), $post = null ) {
        if ( empty( $userid ) || empty( $meta ) || empty( $post ) ) return;
        $skip = (array) get_post_meta( $post->ID, '__gmcecred_skip', true );
        $skipup = ( isset( $skip[ 'up' ] ) && ! empty( $skip[ 'up' ] ) );
        $skipdown = ( isset( $skip[ 'down' ] ) && ! empty( $skip[ 'down' ] ) );
        if ( ($up_down === 'up') && ! $skipup ) {
            do_action( "gmce_approval_reward", $userid );
        } elseif ( ($up_down === 'down') && ! $skipdown ) {
            do_action( "gmce_revision_reward", $userid );
        }
    }

    function thumb_vote( $data ) {
        if ( ! current_filter() === 'gmce_thumb_voted' ) return FALSE;
        if ( ! is_array( $data ) ) return FALSE;
        foreach ( array ( 'what', 'dir', 'from', 'to', 'for' ) as $k ) {
            if ( ! isset( $data[ $k ] ) || empty( $data[ $k ] ) ) return FALSE;
        }
        $to = new \WP_User( $data[ 'to' ] );
        if ( ! $to->exists() ) return FALSE;
        $format = __( '%s for GM Community Editor thumb vote', 'gmcecred' );
        if ( $data[ 'what' ] === "voting_up" || $data[ 'what' ] === "voting_down" ) {
            $event = $data[ 'dir' ] > 0 ? __( 'Reward', 'gmcecred' ) : __( 'Punish', 'gmcecred' );
        } else {
            $event = $data[ 'dir' ] > 0 ? __( 'Undo Punish', 'gmcecred' ) : __( 'Undo Reward', 'gmcecred' );
        }
        $log = sprintf( $format, $event );
        $e = mycred();
        $e->add_creds( 'gmce_' . $event . '_for_thumbvote', $to->ID, (int) $data[ 'dir' ], $log );
    }

}
