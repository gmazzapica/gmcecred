<?php
/**
 * Plugin Name: GMCE Cred
 * Description: GM Community Editor extension that link it to myCred plugins. Require both other plugins, of course.
 * Version: 1.2.0
 * Author: Giuseppe Mazzapica
 * License: GPLv2+
 */
if ( ! defined( 'WPINC' ) ) die();

$path = dirname( __FILE__ );
require( $path . '/loader.php');
$loader = new GMCECredLoader( 'GMCECred', $path . '/src' );
$loader->register();

$hooks = new \GMCECred\Hooks;
$box = new \GMCECred\Box;
$adminbox = new \GMCECred\AdminBox;
$bootstrap = new \GMCECred\Bootstrap( $hooks, $box, $adminbox );
$bootstrap->set_path( $path );
$bootstrap->init();
