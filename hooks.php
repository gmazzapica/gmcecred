<?php
$hooks = array (
    'gmce_reward_approval' => array (
        'title'       => __( 'Reward Community Editor for good approval', 'gmcecred' ),
        'description' => __( 'Reward Community Editor for good approval', 'gmcecred' ),
        'callback'    => array ( '\GMCECred\Hooks\RewardApproval' )
    ),
    'gmce_punish_approval' => array (
        'title'       => __( 'Punish Community Editor for bad approval', 'gmcecred' ),
        'description' => __( 'Punish Community Editor for bad approval', 'gmcecred' ),
        'callback'    => array ( '\GMCECred\Hooks\PunishApproval' )
    ),
    'gmce_reward_revision' => array (
        'title'       => __( 'Reward Community Editor for good revision', 'gmcecred' ),
        'description' => __( 'Reward Community Editor for good revision', 'gmcecred' ),
        'callback'    => array ( '\GMCECred\Hooks\RewardRevision' )
    ),
    'gmce_punish_revision' => array (
        'title'       => __( 'Punish Community Editor for bad revision', 'gmcecred' ),
        'description' => __( 'Punish Community Editor for bad revision', 'gmcecred' ),
        'callback'    => array ( '\GMCECred\Hooks\PunishRevision' )
    )
);
