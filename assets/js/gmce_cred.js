(function($) {

    $(document).on('click', 'a.gmce_approve_punish', function(e) {
        e.preventDefault();
        var $user = $(this).closest('.gmce_approver').find('.gmce_approver_link a').eq(0);
        var username = $user.text();
        var uid = $user.data('uid');
        var $conf = $('#gmce_remove_approval_confirm');
        $conf.data('uid', uid);
        $conf.data('punish', 1);
        var msg = GMCECredData.conf_punish_approve.replace('%s', username);
        GMCommunityEditorBox.openConfirm($conf, msg);
    });


    $(document).on('gmce_remove_approval', function(obj, eventData) {
        var $conf = $('#gmce_remove_approval_confirm');
        var uid = eventData.uid;
        var punish = $conf.data('punish');
        $conf.data('punish', null);
        if (punish === 1) {
            $.ajax({data: {action: 'GMCECred\\Hooks.approval_punish', gmce_uid: uid}});
        }
    });


    $(document).on('click', 'a.gmce_rev_punish', function(e) {
        e.preventDefault();
        var $conf = $(this).closest('.gmce_revision').find('.gmce_action_confirm').eq(0);
        $conf.data('punish', 1);
        var msg = GMCECredData.conf_punish_rev;
        GMCommunityEditorBox.openConfirm($conf, msg);
    });


    $(document).on('gmce_revision_remove', function(obj, eventData) {
        var $conf = $('#gmce_confirm_' + eventData.revid);
        var uid = eventData.uid;
        var punish = $conf.data('punish');
        $conf.data('punish', null);
        if (punish === 1) {
            $.ajax({data: {action: 'GMCECred\\Hooks.revision_punish', gmce_uid: uid}});
        }
    });

})(jQuery);


